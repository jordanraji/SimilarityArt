# -*- coding: utf-8 -*-
import cv2
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from scipy.spatial.distance import euclidean
import matplotlib.pyplot as plt
import os
import sys
import glob
import marshal
import msgpack
import msgpack_numpy

PATH_TRAINING_IMAGES = "/Users/raji/Documents/PT/SimilarityArt/Final/dataset/train/"
TRAINING_IMAGES = [os.path.basename(x) for x in glob.glob(PATH_TRAINING_IMAGES + "*.jpg")]

#HELPERS

def serialize(data, filename):
    fileOut = open(filename, "wb")
    marshal.dump(data, fileOut)
    fileOut.close()

def deserialize(filename):
    fileIn = open(filename, "rb")
    dataLoad = marshal.load(fileIn)
    fileIn.close()
    return dataLoad

def serializeNPArray(data, filename):
    fileOut = open(filename, "wb")
    x_enc = msgpack.packb(data, default=msgpack_numpy.encode)
    fileOut.write(x_enc)
    fileOut.close()

def deserializeNPArray(filename):
    with open(filename, mode='rb') as file: # b is important -> binary
        fileContent = file.read()
        x_rec = msgpack.unpackb(fileContent, object_hook=msgpack_numpy.decode)
        return x_rec


#SIFT 

def show_img_plt(img):
    plt.imshow(cv2.cvtColor(img, cv2.CV_32S))
    plt.show()

def show_img_cv(img):
    cv2.imshow('Image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
def to_gray(img_color):
    gray = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)
    return gray

def gen_sift_features(gray_img):
    sift = cv2.xfeatures2d.SIFT_create()
    kp, desc = sift.detectAndCompute(gray_img, None)
    return kp, desc

def show_sift_features(gray_img, color_img, kp):
    plt.imshow(cv2.drawKeypoints(gray_img, kp, color_img.copy()))
    plt.show()

def matcher(img1, img2):
    bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
    img1_gray = to_gray(img1)
    img2_gray = to_gray(img2)
    img1_kp, img1_desc = gen_sift_features(img1_gray)
    img2_kp, img2_desc = gen_sift_features(img2_gray)
    matches = bf.match(img1_desc, img2_desc)
    matches = sorted(matches, key = lambda x:x.distance)
#    N_MATCHES = min(len(img1_kp),len(img2_kp))
    N_MATCHES = 20
    match_img = cv2.drawMatches(
        img1, img1_kp,
        img2, img2_kp,
        matches[:N_MATCHES], img2.copy(), flags=0)
    plt.figure(figsize=(12,6))
    plt.imshow(match_img)
    plt.show()
    
def explain_kp(kp):
    print "angulo:", kp.angle
    print "class_id:", kp.class_id
    print "octava:", kp.octave
    print "pt:", kp.pt
    print "respuesta:", kp.response
    print "tamaño:", kp.size

def get_descriptors(image_name):
    img = cv2.imread(PATH_TRAINING_IMAGES + image_name)
    img_gray = to_gray(img)
    img_kp, img_desc = gen_sift_features(img_gray)
    return img_desc

def list_descriptors():
    list = []
#    desc = get_descriptors(TRAINING_IMAGES[1])
#    for d in desc:
#        list.append(d)
#    list = np.asarray(list)
    for item in TRAINING_IMAGES:
        desc = get_descriptors(item)
        for d in desc:
            list.append(d)
    list = np.asarray(list)
    serializeNPArray(list, "descriptors.dat")
    print len(list)
    print list.shape


#CLUSTERING
#def make_dictionary():
#    DICT_SIZE = 100
#    BOW = cv2.BOWKMeansTrainer(DICT_SIZE)
#    for p in train

def cluster_image_k_means(img_descriptors_list, k):
    kmeans = KMeans(n_clusters=k).fit(img_descriptors_list)
#    labels = kmeans.labels_
#    centroids = kmeans.cluster_centers_
    return kmeans

#    Esta parte visuliza en 2d
#    for i in xrange(k):
#        ds = img_descriptors_list[np.where(labels==i)]
#        plt.plot(ds[:,0],ds[:,1],'o')
#        lines = plt.plot(centroids[i,0],centroids[i,1],'kx')
#        plt.setp(lines,ms=15.0)
#        plt.setp(lines,mew=2.0)
#    plt.show()

def cluster_image_k_means_cv(img_descriptors_list, n_clusters, max_it, epsilon):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, max_it, epsilon)
    ret,label,center = cv2.kmeans(img_descriptors_list, n_clusters, None, criteria, 20, cv2.KMEANS_RANDOM_CENTERS)
    serializeNPArray(center, "dictionary.dat")
    print ret

#BOW
def get_the_index_with_less_value(dictionary, value):
    i = 0
    distances = []
    while(i < len(dictionary)):
        distances.append(euclidean(value, dictionary[i]))
        i += 1
    return distances.index(min(distances))

def build_histogram(dictionary, img_name):
    img = cv2.imread(img_name)
    img_gray = to_gray(img)
    img_kp, img_desc = gen_sift_features(img_gray)
    histogram = np.zeros((len(dictionary),), dtype=np.int)
    for desc in img_desc:
        index = get_the_index_with_less_value(dictionary, desc)
        histogram[index] += 1
    return histogram


def main():
    print "OpenCV version: " + cv2.__version__
    print "OpenCV is Optimized: " + str(cv2.useOptimized())

#    img1 = cv2.imread('starry.jpg')
#    img2 = cv2.imread('starry2.jpg')
#    matcher(img1, img2)
#
#    img1_gray = to_gray(img1)
#    img1_kp, img1_desc = gen_sift_features(img1_gray)


#ELBOWS METHOD
#    Ks = range(1, 40)
#    km = [KMeans(n_clusters=i) for i in Ks]
#    score = [km[i].fit(img1_desc).score(img1_desc) for i in range(len(km))]
#    
#    list_len = list(range(1,40))
#    plt.plot(list_len, score)
#    plt.show()


#CREA VECTOR DE CARACTERISTICAS
#    list_descriptors()


#METODO DE LA SILUETA
#    des = deserializeNPArray("descriptors.dat")
#    K = 700
#    clusterer = KMeans(n_clusters= K)
#    cluster_labels = clusterer.fit_predict(des)
#    silhouette_avg = silhouette_score(des, cluster_labels)
#    print("For n_clusters =", K,
#          "The average silhouette_score is :", silhouette_avg)



#ESTAS LINEA CREA EL DICCIONARIO
#    des = deserializeNPArray("descriptors.dat")
#    cluster_image_k_means_cv(des, 250, 100, 0.8)

#==========
#    des = deserializeNPArray("descriptors.dat")
#    cluster_image_k_means(des, 600)

#EJEMPLO
#    show_sift_features(img1_gray, img1, img1_kp)

#CARGAR DICCIONARIO
#
#    dictionary = deserializeNPArray("dictionary10_250.dat")
#    index = get_the_index_with_less_value(dictionary, dictionary[150])
#    histogram = np.zeros((len(dictionary),), dtype=np.int)
#    histogram[index]+=1
#    print histogram

    dictionary = deserializeNPArray("dictionary10_250.dat")
    histogram1 = build_histogram(dictionary, "starry.jpg")
    histogram2 = build_histogram(dictionary, "starry.jpg")
    print euclidean(histogram1, histogram2)

if __name__ == "__main__":
    main()
